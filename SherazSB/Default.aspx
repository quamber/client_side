﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="QuamberAWS.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SQS Inbox/Outbox</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/custom.css" />
</head>
<body>
    <form id="form1" runat="server">
              
        <div class="main_set">
            <h2>Cloud Project</h2>
            <asp:DropDownList runat="server" ID="ddlFunctions" AutoPostBack="true">
                 <asp:ListItem Value="toLowerCase" Text="toLowerCase"></asp:ListItem>
                <asp:ListItem Value="toUpperCase" Text="toUpperCase"></asp:ListItem>
                <asp:ListItem Value="Reverse" Text="Reverse"></asp:ListItem>
                <asp:ListItem Value="Capitalize" Text="Capitalize"></asp:ListItem>
           </asp:DropDownList>

             <asp:TextBox TextMode="MultiLine" runat="server" ID="txtText" required placeholder="Type Message Here"></asp:TextBox>
            <asp:Button CssClass="button1" runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="SOAP Request" />
            <asp:Button CssClass="button2" runat="server" ID="btnAWSSQSRequest" Text="SQS Request" OnClick="btnAWSSQS_Click" />
         
             <asp:Button CssClass="button3" runat="server" ID="btnAWSSQSResponse" Text="SQS Response" OnClick="btnAWSSQSResponse_Click"  />

            <asp:Panel CssClass="aler" runat="server" ID="pnlError" Visible="false">
               	<span><img src="img/question.png" alt="" ></span><asp:Label runat="server" ID="lblError"></asp:Label>
            </asp:Panel>

            <div class="msg_dip">
                <h2>Result</h2>
                 <asp:Label runat="server" ID="lblResponse"></asp:Label><br />
                <asp:Label runat="server" ID="lbl_Timer"></asp:Label>
            </div>
        </div>

           <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:Timer ID="Timer1" OnTick="Timer1_Tick" runat="server" Interval="2000" />

        <asp:UpdatePanel ID="StockPricePanel" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" />
            </Triggers>
            <ContentTemplate>
                
            </ContentTemplate>
        </asp:UpdatePanel>--%>
    </form>
</body>
</html>
