﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuamberAWS.AmazonService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://quamber.net", ConfigurationName="AmazonService.ServiceSoap")]
    public interface ServiceSoap {
        
        // CODEGEN: Generating message contract since element name functionName from namespace http://quamber.net is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://quamber.net/ProcessCall", ReplyAction="*")]
        QuamberAWS.AmazonService.ProcessCallResponse ProcessCall(QuamberAWS.AmazonService.ProcessCallRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://quamber.net/ProcessCall", ReplyAction="*")]
        System.Threading.Tasks.Task<QuamberAWS.AmazonService.ProcessCallResponse> ProcessCallAsync(QuamberAWS.AmazonService.ProcessCallRequest request);
        
        // CODEGEN: Generating message contract since element name messageID from namespace http://quamber.net is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://quamber.net/OutboxMessageRead", ReplyAction="*")]
        QuamberAWS.AmazonService.OutboxMessageReadResponse OutboxMessageRead(QuamberAWS.AmazonService.OutboxMessageReadRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://quamber.net/OutboxMessageRead", ReplyAction="*")]
        System.Threading.Tasks.Task<QuamberAWS.AmazonService.OutboxMessageReadResponse> OutboxMessageReadAsync(QuamberAWS.AmazonService.OutboxMessageReadRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ProcessCallRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ProcessCall", Namespace="http://quamber.net", Order=0)]
        public QuamberAWS.AmazonService.ProcessCallRequestBody Body;
        
        public ProcessCallRequest() {
        }
        
        public ProcessCallRequest(QuamberAWS.AmazonService.ProcessCallRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://quamber.net")]
    public partial class ProcessCallRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string functionName;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string data;
        
        public ProcessCallRequestBody() {
        }
        
        public ProcessCallRequestBody(string functionName, string data) {
            this.functionName = functionName;
            this.data = data;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ProcessCallResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ProcessCallResponse", Namespace="http://quamber.net", Order=0)]
        public QuamberAWS.AmazonService.ProcessCallResponseBody Body;
        
        public ProcessCallResponse() {
        }
        
        public ProcessCallResponse(QuamberAWS.AmazonService.ProcessCallResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://quamber.net")]
    public partial class ProcessCallResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public System.Xml.Linq.XElement ProcessCallResult;
        
        public ProcessCallResponseBody() {
        }
        
        public ProcessCallResponseBody(System.Xml.Linq.XElement ProcessCallResult) {
            this.ProcessCallResult = ProcessCallResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class OutboxMessageReadRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="OutboxMessageRead", Namespace="http://quamber.net", Order=0)]
        public QuamberAWS.AmazonService.OutboxMessageReadRequestBody Body;
        
        public OutboxMessageReadRequest() {
        }
        
        public OutboxMessageReadRequest(QuamberAWS.AmazonService.OutboxMessageReadRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://quamber.net")]
    public partial class OutboxMessageReadRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string messageID;
        
        public OutboxMessageReadRequestBody() {
        }
        
        public OutboxMessageReadRequestBody(string messageID) {
            this.messageID = messageID;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class OutboxMessageReadResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="OutboxMessageReadResponse", Namespace="http://quamber.net", Order=0)]
        public QuamberAWS.AmazonService.OutboxMessageReadResponseBody Body;
        
        public OutboxMessageReadResponse() {
        }
        
        public OutboxMessageReadResponse(QuamberAWS.AmazonService.OutboxMessageReadResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://quamber.net")]
    public partial class OutboxMessageReadResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public System.Xml.Linq.XElement OutboxMessageReadResult;
        
        public OutboxMessageReadResponseBody() {
        }
        
        public OutboxMessageReadResponseBody(System.Xml.Linq.XElement OutboxMessageReadResult) {
            this.OutboxMessageReadResult = OutboxMessageReadResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ServiceSoapChannel : QuamberAWS.AmazonService.ServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceSoapClient : System.ServiceModel.ClientBase<QuamberAWS.AmazonService.ServiceSoap>, QuamberAWS.AmazonService.ServiceSoap {
        
        public ServiceSoapClient() {
        }
        
        public ServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        QuamberAWS.AmazonService.ProcessCallResponse QuamberAWS.AmazonService.ServiceSoap.ProcessCall(QuamberAWS.AmazonService.ProcessCallRequest request) {
            return base.Channel.ProcessCall(request);
        }
        
        public System.Xml.Linq.XElement ProcessCall(string functionName, string data) {
            QuamberAWS.AmazonService.ProcessCallRequest inValue = new QuamberAWS.AmazonService.ProcessCallRequest();
            inValue.Body = new QuamberAWS.AmazonService.ProcessCallRequestBody();
            inValue.Body.functionName = functionName;
            inValue.Body.data = data;
            QuamberAWS.AmazonService.ProcessCallResponse retVal = ((QuamberAWS.AmazonService.ServiceSoap)(this)).ProcessCall(inValue);
            return retVal.Body.ProcessCallResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<QuamberAWS.AmazonService.ProcessCallResponse> QuamberAWS.AmazonService.ServiceSoap.ProcessCallAsync(QuamberAWS.AmazonService.ProcessCallRequest request) {
            return base.Channel.ProcessCallAsync(request);
        }
        
        public System.Threading.Tasks.Task<QuamberAWS.AmazonService.ProcessCallResponse> ProcessCallAsync(string functionName, string data) {
            QuamberAWS.AmazonService.ProcessCallRequest inValue = new QuamberAWS.AmazonService.ProcessCallRequest();
            inValue.Body = new QuamberAWS.AmazonService.ProcessCallRequestBody();
            inValue.Body.functionName = functionName;
            inValue.Body.data = data;
            return ((QuamberAWS.AmazonService.ServiceSoap)(this)).ProcessCallAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        QuamberAWS.AmazonService.OutboxMessageReadResponse QuamberAWS.AmazonService.ServiceSoap.OutboxMessageRead(QuamberAWS.AmazonService.OutboxMessageReadRequest request) {
            return base.Channel.OutboxMessageRead(request);
        }
        
        public System.Xml.Linq.XElement OutboxMessageRead(string messageID) {
            QuamberAWS.AmazonService.OutboxMessageReadRequest inValue = new QuamberAWS.AmazonService.OutboxMessageReadRequest();
            inValue.Body = new QuamberAWS.AmazonService.OutboxMessageReadRequestBody();
            inValue.Body.messageID = messageID;
            QuamberAWS.AmazonService.OutboxMessageReadResponse retVal = ((QuamberAWS.AmazonService.ServiceSoap)(this)).OutboxMessageRead(inValue);
            return retVal.Body.OutboxMessageReadResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<QuamberAWS.AmazonService.OutboxMessageReadResponse> QuamberAWS.AmazonService.ServiceSoap.OutboxMessageReadAsync(QuamberAWS.AmazonService.OutboxMessageReadRequest request) {
            return base.Channel.OutboxMessageReadAsync(request);
        }
        
        public System.Threading.Tasks.Task<QuamberAWS.AmazonService.OutboxMessageReadResponse> OutboxMessageReadAsync(string messageID) {
            QuamberAWS.AmazonService.OutboxMessageReadRequest inValue = new QuamberAWS.AmazonService.OutboxMessageReadRequest();
            inValue.Body = new QuamberAWS.AmazonService.OutboxMessageReadRequestBody();
            inValue.Body.messageID = messageID;
            return ((QuamberAWS.AmazonService.ServiceSoap)(this)).OutboxMessageReadAsync(inValue);
        }
    }
}
