﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Windows.Forms;
using System.Xml;
using Amazon.Runtime;
using Amazon.SQS;
using Amazon;
using Amazon.SQS.Model;

namespace QuamberAWS
{
    public partial class Default : System.Web.UI.Page
    {
          protected void Lbl_Console(String _response)
    {
                pnlError.Visible = false;
                lblError.Text = "";
                String temp = lblResponse.Text;
                lblResponse.Text = "<br />" + _response + "<br />";
                lblResponse.Text += "<br />-----------------------------";
                lblResponse.Text +="<br />"+ temp+"<br />";
              

    }
          protected void Lbl_Error(String error)
          {
              pnlError.Visible = true;
              lblError.Text = error;
              lblResponse.Text = "";
             
          }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        int counter = 0;


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                String text = txtText.Text;
                String Func = ddlFunctions.SelectedValue;

                AmazonLocal.ServiceSoapClient client = new AmazonLocal.ServiceSoapClient();
                XElement ele = client.ProcessCall(Func, text);
                XmlDocument xD = new XmlDocument();
                xD.LoadXml(ele.ToString());
                String Error = CheckError(xD);
                if (Error != "")
                {
                    pnlError.Visible = true;
                    lblError.Text = Error;
                    lblResponse.Text = "";
                }
                else
                {
                    pnlError.Visible = false;
                    lblError.Text = "";
                    lblResponse.Text = xD.FirstChild.ChildNodes[2].InnerText;
                }
            }
            catch (Exception ex)
            {
                pnlError.Visible = true;
                lblError.Text = ex.Message;
                lblResponse.Text = "";
            }

        }
        private String CheckError(XmlDocument xd)
        {
            foreach (System.Xml.XmlElement item in xd.FirstChild.ChildNodes)
            {
                if (item.Name == "Exception")
                    return item.InnerText;
            }
            return "";
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            //txtResponse.Text = counter.ToString();
            counter++;
            Lbl_Console(counter.ToString());
        }

        protected void btnAWSSQS_Click(object sender, EventArgs e)
        {
            Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

            AWSCredentials Credentials = new StoredProfileAWSCredentials("QuamberAli");

            var sqs = new AmazonSQSClient(Credentials, RegionEndpoint.USWest2);
            try
            {

                //Creating a queue
                var sqsRequest = new CreateQueueRequest { QueueName = "PungaInbox" };
                var createQueueResponse = sqs.CreateQueue(sqsRequest);
                string myQueueUrl = createQueueResponse.QueueUrl;
                String functionName = ddlFunctions.SelectedValue;
                String data = txtText.Text.Trim();

                ////Sending a message
                Dictionary<String, MessageAttributeValue> messageAttributesIB = new Dictionary<String, MessageAttributeValue>();

                MessageAttributeValue atrIB = new MessageAttributeValue();
                atrIB.StringValue = functionName;
                atrIB.DataType = "String";
                messageAttributesIB.Add("function", atrIB);
                var sendMessageRequest = new SendMessageRequest
                {
                    QueueUrl = myQueueUrl, //URL from initial queue creation
                    MessageBody = data,
                    MessageAttributes = messageAttributesIB

                };
                Lbl_Console("Sending Message");
                SendMessageResponse responce = sqs.SendMessage(sendMessageRequest);

                //Receiving a message

                if (responce == null)
                { Lbl_Error("Sending Message: No Respose Received "); }
                else {

                    Lbl_Console("Message Sent");
                    Lbl_Console("Sent Message SQS ID"+responce.MessageId);
                }
            }
            catch (Exception ex) { Lbl_Error(ex.Message); }
          
        }

        protected void btnAWSSQSResponse_Click(object sender, EventArgs e)
        {
            try
            {

                Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

                AWSCredentials CredentialsWB = new StoredProfileAWSCredentials("QuamberAli");
                String outputID = "";
                var sqsWB = new AmazonSQSClient(CredentialsWB, RegionEndpoint.USWest2);
                var sqsRequestWB = new CreateQueueRequest { QueueName = "PungaOutbox" };
                var createQueueResponseWB = sqsWB.CreateQueue(sqsRequestWB);
                string myQueueUrlWB = createQueueResponseWB.QueueUrl;
                var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = myQueueUrlWB };
                receiveMessageRequest.MaxNumberOfMessages = 1;
                var receiveMessageResponse = sqsWB.ReceiveMessage(receiveMessageRequest);
                Lbl_Console("Receiing message.<br />");
                if (receiveMessageResponse.Messages != null)
                {
                    if (receiveMessageResponse.Messages.Count == 0)
                    {
                        Lbl_Console("No Message Received");
                    }
                    foreach (var message in receiveMessageResponse.Messages)
                    {
                        if (!string.IsNullOrEmpty(message.Body))
                        {

                            Lbl_Console("Message Received Is :" + message.Body);
                            Lbl_Console("Deleting the message.<br />");
                            var messageRecieptHandle = message.ReceiptHandle;

                            var deleteRequest = new DeleteMessageRequest { QueueUrl = myQueueUrlWB, ReceiptHandle = messageRecieptHandle };
                            sqsWB.DeleteMessage(deleteRequest);
                        }

                    }
                }


            }

            catch (AmazonSQSException ex)
            {
                Lbl_Console("Caught Exception: " + ex.Message);
                Lbl_Console("Response Status Code: " + ex.StatusCode);
                Lbl_Console("Error Code: " + ex.ErrorCode);
                Lbl_Console("Error Type: " + ex.ErrorType);
                Lbl_Console("Request ID: " + ex.RequestId);

            }
        }
    }
}